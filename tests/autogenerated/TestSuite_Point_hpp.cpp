#include "cpptest.h"

/* CPPTEST_TEST_SUITE_CODE_BEGIN AdditionalIncludes */
/* CPPTEST_TEST_SUITE_CODE_END AdditionalIncludes */

CPPTEST_CONTEXT("/FlowAnalysisCpp/Point.hpp");
CPPTEST_TEST_SUITE_INCLUDED_TO("/FlowAnalysisCpp/DeadLock.cpp");

class TestSuite_Point_hpp_521bf514 : public CppTest_TestSuite
{
    public:
        CPPTEST_TEST_SUITE(TestSuite_Point_hpp_521bf514);
        CPPTEST_TEST_SUITE_SETUP(testSuiteSetUp);
        CPPTEST_TEST(test_Point_1);
        CPPTEST_TEST(test_Point_10);
        CPPTEST_TEST(test_Point_2);
        CPPTEST_TEST(test_Point_3);
        CPPTEST_TEST(test_Point_4);
        CPPTEST_TEST(test_Point_5);
        CPPTEST_TEST(test_Point_6);
        CPPTEST_TEST(test_Point_7);
        CPPTEST_TEST(test_Point_8);
        CPPTEST_TEST(test_Point_9);
        CPPTEST_TEST(test_reflectAcrossX_1);
        CPPTEST_TEST(test_reflectAcrossX_10);
        CPPTEST_TEST(test_reflectAcrossX_2);
        CPPTEST_TEST(test_reflectAcrossX_3);
        CPPTEST_TEST(test_reflectAcrossX_4);
        CPPTEST_TEST(test_reflectAcrossX_5);
        CPPTEST_TEST(test_reflectAcrossX_6);
        CPPTEST_TEST(test_reflectAcrossX_7);
        CPPTEST_TEST(test_reflectAcrossX_8);
        CPPTEST_TEST(test_reflectAcrossX_9);
        CPPTEST_TEST(test_squareDistanceTo_1);
        CPPTEST_TEST(test_squareDistanceTo_10);
        CPPTEST_TEST(test_squareDistanceTo_2);
        CPPTEST_TEST(test_squareDistanceTo_3);
        CPPTEST_TEST(test_squareDistanceTo_4);
        CPPTEST_TEST(test_squareDistanceTo_5);
        CPPTEST_TEST(test_squareDistanceTo_6);
        CPPTEST_TEST(test_squareDistanceTo_7);
        CPPTEST_TEST(test_squareDistanceTo_8);
        CPPTEST_TEST(test_squareDistanceTo_9);
        CPPTEST_TEST(test_translate_1);
        CPPTEST_TEST(test_translate_10);
        CPPTEST_TEST(test_translate_2);
        CPPTEST_TEST(test_translate_3);
        CPPTEST_TEST(test_translate_4);
        CPPTEST_TEST(test_translate_5);
        CPPTEST_TEST(test_translate_6);
        CPPTEST_TEST(test_translate_7);
        CPPTEST_TEST(test_translate_8);
        CPPTEST_TEST(test_translate_9);
        CPPTEST_TEST_SUITE_TEARDOWN(testSuiteTearDown);
        CPPTEST_TEST_SUITE_END();
        
        static void testSuiteSetUp();
        static void testSuiteTearDown();

        void setUp();
        void tearDown();

        void test_Point_1();
        void test_Point_10();
        void test_Point_2();
        void test_Point_3();
        void test_Point_4();
        void test_Point_5();
        void test_Point_6();
        void test_Point_7();
        void test_Point_8();
        void test_Point_9();
        void test_reflectAcrossX_1();
        void test_reflectAcrossX_10();
        void test_reflectAcrossX_2();
        void test_reflectAcrossX_3();
        void test_reflectAcrossX_4();
        void test_reflectAcrossX_5();
        void test_reflectAcrossX_6();
        void test_reflectAcrossX_7();
        void test_reflectAcrossX_8();
        void test_reflectAcrossX_9();
        void test_squareDistanceTo_1();
        void test_squareDistanceTo_10();
        void test_squareDistanceTo_2();
        void test_squareDistanceTo_3();
        void test_squareDistanceTo_4();
        void test_squareDistanceTo_5();
        void test_squareDistanceTo_6();
        void test_squareDistanceTo_7();
        void test_squareDistanceTo_8();
        void test_squareDistanceTo_9();
        void test_translate_1();
        void test_translate_10();
        void test_translate_2();
        void test_translate_3();
        void test_translate_4();
        void test_translate_5();
        void test_translate_6();
        void test_translate_7();
        void test_translate_8();
        void test_translate_9();
};

CPPTEST_TEST_SUITE_REGISTRATION(TestSuite_Point_hpp_521bf514);

void TestSuite_Point_hpp_521bf514::testSuiteSetUp()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestSuiteSetUp */
/* CPPTEST_TEST_SUITE_CODE_END TestSuiteSetUp */
}

void TestSuite_Point_hpp_521bf514::testSuiteTearDown()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestSuiteTearDown */
/* CPPTEST_TEST_SUITE_CODE_END TestSuiteTearDown */
}

void TestSuite_Point_hpp_521bf514::setUp()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestCaseSetUp */
/* CPPTEST_TEST_SUITE_CODE_END TestCaseSetUp */
}

void TestSuite_Point_hpp_521bf514::tearDown()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestCaseTearDown */
/* CPPTEST_TEST_SUITE_CODE_END TestCaseTearDown */
}

/* CPPTEST_TEST_CASE_BEGIN test_Point_1 */
/* CPPTEST_TEST_CASE_CONTEXT Point::Point(int, int) */
void TestSuite_Point_hpp_521bf514::test_Point_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (x) */ 
    int _x  = cpptestLimitsGetMaxInt();
    /* Initializing argument 2 (y) */ 
    int _y  = cpptestLimitsGetMaxInt();
    /* Testing constructor. */
    ::Point _return(_x, _y);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return._x", ( _return._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _return._y", ( _return._y ));
}
/* CPPTEST_TEST_CASE_END test_Point_1 */

/* CPPTEST_TEST_CASE_BEGIN test_Point_10 */
/* CPPTEST_TEST_CASE_CONTEXT Point::Point(int, int) */
void TestSuite_Point_hpp_521bf514::test_Point_10()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (x) */ 
    int _x  = -1;
    /* Initializing argument 2 (y) */ 
    int _y  = 0;
    /* Testing constructor. */
    ::Point _return(_x, _y);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return._x", ( _return._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _return._y", ( _return._y ));
}
/* CPPTEST_TEST_CASE_END test_Point_10 */

/* CPPTEST_TEST_CASE_BEGIN test_Point_2 */
/* CPPTEST_TEST_CASE_CONTEXT Point::Point(int, int) */
void TestSuite_Point_hpp_521bf514::test_Point_2()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (x) */ 
    int _x  = cpptestLimitsGetMinInt();
    /* Initializing argument 2 (y) */ 
    int _y  = cpptestLimitsGetMaxInt();
    /* Testing constructor. */
    ::Point _return(_x, _y);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return._x", ( _return._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _return._y", ( _return._y ));
}
/* CPPTEST_TEST_CASE_END test_Point_2 */

/* CPPTEST_TEST_CASE_BEGIN test_Point_3 */
/* CPPTEST_TEST_CASE_CONTEXT Point::Point(int, int) */
void TestSuite_Point_hpp_521bf514::test_Point_3()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (x) */ 
    int _x  = 1;
    /* Initializing argument 2 (y) */ 
    int _y  = cpptestLimitsGetMaxInt();
    /* Testing constructor. */
    ::Point _return(_x, _y);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return._x", ( _return._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _return._y", ( _return._y ));
}
/* CPPTEST_TEST_CASE_END test_Point_3 */

/* CPPTEST_TEST_CASE_BEGIN test_Point_4 */
/* CPPTEST_TEST_CASE_CONTEXT Point::Point(int, int) */
void TestSuite_Point_hpp_521bf514::test_Point_4()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (x) */ 
    int _x  = cpptestLimitsGetMaxInt();
    /* Initializing argument 2 (y) */ 
    int _y  = cpptestLimitsGetMinInt();
    /* Testing constructor. */
    ::Point _return(_x, _y);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return._x", ( _return._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _return._y", ( _return._y ));
}
/* CPPTEST_TEST_CASE_END test_Point_4 */

/* CPPTEST_TEST_CASE_BEGIN test_Point_5 */
/* CPPTEST_TEST_CASE_CONTEXT Point::Point(int, int) */
void TestSuite_Point_hpp_521bf514::test_Point_5()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (x) */ 
    int _x  = -1;
    /* Initializing argument 2 (y) */ 
    int _y  = cpptestLimitsGetMinInt();
    /* Testing constructor. */
    ::Point _return(_x, _y);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return._x", ( _return._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _return._y", ( _return._y ));
}
/* CPPTEST_TEST_CASE_END test_Point_5 */

/* CPPTEST_TEST_CASE_BEGIN test_Point_6 */
/* CPPTEST_TEST_CASE_CONTEXT Point::Point(int, int) */
void TestSuite_Point_hpp_521bf514::test_Point_6()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (x) */ 
    int _x  = cpptestLimitsGetMaxInt();
    /* Initializing argument 2 (y) */ 
    int _y  = -1;
    /* Testing constructor. */
    ::Point _return(_x, _y);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return._x", ( _return._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _return._y", ( _return._y ));
}
/* CPPTEST_TEST_CASE_END test_Point_6 */

/* CPPTEST_TEST_CASE_BEGIN test_Point_7 */
/* CPPTEST_TEST_CASE_CONTEXT Point::Point(int, int) */
void TestSuite_Point_hpp_521bf514::test_Point_7()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (x) */ 
    int _x  = 1;
    /* Initializing argument 2 (y) */ 
    int _y  = -1;
    /* Testing constructor. */
    ::Point _return(_x, _y);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return._x", ( _return._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _return._y", ( _return._y ));
}
/* CPPTEST_TEST_CASE_END test_Point_7 */

/* CPPTEST_TEST_CASE_BEGIN test_Point_8 */
/* CPPTEST_TEST_CASE_CONTEXT Point::Point(int, int) */
void TestSuite_Point_hpp_521bf514::test_Point_8()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (x) */ 
    int _x  = cpptestLimitsGetMinInt();
    /* Initializing argument 2 (y) */ 
    int _y  = 1;
    /* Testing constructor. */
    ::Point _return(_x, _y);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return._x", ( _return._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _return._y", ( _return._y ));
}
/* CPPTEST_TEST_CASE_END test_Point_8 */

/* CPPTEST_TEST_CASE_BEGIN test_Point_9 */
/* CPPTEST_TEST_CASE_CONTEXT Point::Point(int, int) */
void TestSuite_Point_hpp_521bf514::test_Point_9()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (x) */ 
    int _x  = 0;
    /* Initializing argument 2 (y) */ 
    int _y  = 1;
    /* Testing constructor. */
    ::Point _return(_x, _y);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return._x", ( _return._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _return._y", ( _return._y ));
}
/* CPPTEST_TEST_CASE_END test_Point_9 */

/* CPPTEST_TEST_CASE_BEGIN test_reflectAcrossX_1 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::reflectAcrossX(void) */
void TestSuite_Point_hpp_521bf514::test_reflectAcrossX_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Tested function call */
    _cpptest_TestObject.reflectAcrossX();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_reflectAcrossX_1 */

/* CPPTEST_TEST_CASE_BEGIN test_reflectAcrossX_10 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::reflectAcrossX(void) */
void TestSuite_Point_hpp_521bf514::test_reflectAcrossX_10()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = -1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 0;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Tested function call */
    _cpptest_TestObject.reflectAcrossX();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_reflectAcrossX_10 */

/* CPPTEST_TEST_CASE_BEGIN test_reflectAcrossX_2 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::reflectAcrossX(void) */
void TestSuite_Point_hpp_521bf514::test_reflectAcrossX_2()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Tested function call */
    _cpptest_TestObject.reflectAcrossX();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_reflectAcrossX_2 */

/* CPPTEST_TEST_CASE_BEGIN test_reflectAcrossX_3 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::reflectAcrossX(void) */
void TestSuite_Point_hpp_521bf514::test_reflectAcrossX_3()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = 1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Tested function call */
    _cpptest_TestObject.reflectAcrossX();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_reflectAcrossX_3 */

/* CPPTEST_TEST_CASE_BEGIN test_reflectAcrossX_4 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::reflectAcrossX(void) */
void TestSuite_Point_hpp_521bf514::test_reflectAcrossX_4()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMinInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Tested function call */
    _cpptest_TestObject.reflectAcrossX();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_reflectAcrossX_4 */

/* CPPTEST_TEST_CASE_BEGIN test_reflectAcrossX_5 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::reflectAcrossX(void) */
void TestSuite_Point_hpp_521bf514::test_reflectAcrossX_5()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = -1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMinInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Tested function call */
    _cpptest_TestObject.reflectAcrossX();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_reflectAcrossX_5 */

/* CPPTEST_TEST_CASE_BEGIN test_reflectAcrossX_6 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::reflectAcrossX(void) */
void TestSuite_Point_hpp_521bf514::test_reflectAcrossX_6()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = -1;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Tested function call */
    _cpptest_TestObject.reflectAcrossX();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_reflectAcrossX_6 */

/* CPPTEST_TEST_CASE_BEGIN test_reflectAcrossX_7 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::reflectAcrossX(void) */
void TestSuite_Point_hpp_521bf514::test_reflectAcrossX_7()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = 1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = -1;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Tested function call */
    _cpptest_TestObject.reflectAcrossX();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_reflectAcrossX_7 */

/* CPPTEST_TEST_CASE_BEGIN test_reflectAcrossX_8 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::reflectAcrossX(void) */
void TestSuite_Point_hpp_521bf514::test_reflectAcrossX_8()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 1;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Tested function call */
    _cpptest_TestObject.reflectAcrossX();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_reflectAcrossX_8 */

/* CPPTEST_TEST_CASE_BEGIN test_reflectAcrossX_9 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::reflectAcrossX(void) */
void TestSuite_Point_hpp_521bf514::test_reflectAcrossX_9()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = 0;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 1;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Tested function call */
    _cpptest_TestObject.reflectAcrossX();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_reflectAcrossX_9 */

/* CPPTEST_TEST_CASE_BEGIN test_squareDistanceTo_1 */
/* CPPTEST_TEST_CASE_CONTEXT int Point::squareDistanceTo(const Point &) */
void TestSuite_Point_hpp_521bf514::test_squareDistanceTo_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMaxInt();
    ::Point _point (_x_1, _y_1);

    /* Tested function call */
    int _return  = _cpptest_TestObject.squareDistanceTo(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_squareDistanceTo_1 */

/* CPPTEST_TEST_CASE_BEGIN test_squareDistanceTo_10 */
/* CPPTEST_TEST_CASE_CONTEXT int Point::squareDistanceTo(const Point &) */
void TestSuite_Point_hpp_521bf514::test_squareDistanceTo_10()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMinInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = -1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = 0;
    ::Point _point (_x_1, _y_1);

    /* Tested function call */
    int _return  = _cpptest_TestObject.squareDistanceTo(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_squareDistanceTo_10 */

/* CPPTEST_TEST_CASE_BEGIN test_squareDistanceTo_2 */
/* CPPTEST_TEST_CASE_CONTEXT int Point::squareDistanceTo(const Point &) */
void TestSuite_Point_hpp_521bf514::test_squareDistanceTo_2()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMaxInt();
    ::Point _point (_x_1, _y_1);

    /* Tested function call */
    int _return  = _cpptest_TestObject.squareDistanceTo(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_squareDistanceTo_2 */

/* CPPTEST_TEST_CASE_BEGIN test_squareDistanceTo_3 */
/* CPPTEST_TEST_CASE_CONTEXT int Point::squareDistanceTo(const Point &) */
void TestSuite_Point_hpp_521bf514::test_squareDistanceTo_3()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 0;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = -1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMaxInt();
    ::Point _point (_x_1, _y_1);

    /* Tested function call */
    int _return  = _cpptest_TestObject.squareDistanceTo(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_squareDistanceTo_3 */

/* CPPTEST_TEST_CASE_BEGIN test_squareDistanceTo_4 */
/* CPPTEST_TEST_CASE_CONTEXT int Point::squareDistanceTo(const Point &) */
void TestSuite_Point_hpp_521bf514::test_squareDistanceTo_4()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = 0;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = -1;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMinInt();
    ::Point _point (_x_1, _y_1);

    /* Tested function call */
    int _return  = _cpptest_TestObject.squareDistanceTo(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_squareDistanceTo_4 */

/* CPPTEST_TEST_CASE_BEGIN test_squareDistanceTo_5 */
/* CPPTEST_TEST_CASE_CONTEXT int Point::squareDistanceTo(const Point &) */
void TestSuite_Point_hpp_521bf514::test_squareDistanceTo_5()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = 1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMinInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = 1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMinInt();
    ::Point _point (_x_1, _y_1);

    /* Tested function call */
    int _return  = _cpptest_TestObject.squareDistanceTo(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_squareDistanceTo_5 */

/* CPPTEST_TEST_CASE_BEGIN test_squareDistanceTo_6 */
/* CPPTEST_TEST_CASE_CONTEXT int Point::squareDistanceTo(const Point &) */
void TestSuite_Point_hpp_521bf514::test_squareDistanceTo_6()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = -1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = -1;
    ::Point _point (_x_1, _y_1);

    /* Tested function call */
    int _return  = _cpptest_TestObject.squareDistanceTo(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_squareDistanceTo_6 */

/* CPPTEST_TEST_CASE_BEGIN test_squareDistanceTo_7 */
/* CPPTEST_TEST_CASE_CONTEXT int Point::squareDistanceTo(const Point &) */
void TestSuite_Point_hpp_521bf514::test_squareDistanceTo_7()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 0;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = 1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = -1;
    ::Point _point (_x_1, _y_1);

    /* Tested function call */
    int _return  = _cpptest_TestObject.squareDistanceTo(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_squareDistanceTo_7 */

/* CPPTEST_TEST_CASE_BEGIN test_squareDistanceTo_8 */
/* CPPTEST_TEST_CASE_CONTEXT int Point::squareDistanceTo(const Point &) */
void TestSuite_Point_hpp_521bf514::test_squareDistanceTo_8()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 1;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = 1;
    ::Point _point (_x_1, _y_1);

    /* Tested function call */
    int _return  = _cpptest_TestObject.squareDistanceTo(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_squareDistanceTo_8 */

/* CPPTEST_TEST_CASE_BEGIN test_squareDistanceTo_9 */
/* CPPTEST_TEST_CASE_CONTEXT int Point::squareDistanceTo(const Point &) */
void TestSuite_Point_hpp_521bf514::test_squareDistanceTo_9()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = -1;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = 0;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = 1;
    ::Point _point (_x_1, _y_1);

    /* Tested function call */
    int _return  = _cpptest_TestObject.squareDistanceTo(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_squareDistanceTo_9 */

/* CPPTEST_TEST_CASE_BEGIN test_translate_1 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::translate(const Point &) */
void TestSuite_Point_hpp_521bf514::test_translate_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (vector) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMaxInt();
    ::Point _vector (_x_1, _y_1);

    /* Tested function call */
    _cpptest_TestObject.translate(_vector);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_translate_1 */

/* CPPTEST_TEST_CASE_BEGIN test_translate_10 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::translate(const Point &) */
void TestSuite_Point_hpp_521bf514::test_translate_10()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMinInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (vector) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = -1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = 0;
    ::Point _vector (_x_1, _y_1);

    /* Tested function call */
    _cpptest_TestObject.translate(_vector);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_translate_10 */

/* CPPTEST_TEST_CASE_BEGIN test_translate_2 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::translate(const Point &) */
void TestSuite_Point_hpp_521bf514::test_translate_2()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (vector) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMaxInt();
    ::Point _vector (_x_1, _y_1);

    /* Tested function call */
    _cpptest_TestObject.translate(_vector);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_translate_2 */

/* CPPTEST_TEST_CASE_BEGIN test_translate_3 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::translate(const Point &) */
void TestSuite_Point_hpp_521bf514::test_translate_3()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 0;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (vector) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = -1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMaxInt();
    ::Point _vector (_x_1, _y_1);

    /* Tested function call */
    _cpptest_TestObject.translate(_vector);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_translate_3 */

/* CPPTEST_TEST_CASE_BEGIN test_translate_4 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::translate(const Point &) */
void TestSuite_Point_hpp_521bf514::test_translate_4()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = 0;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = -1;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (vector) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMinInt();
    ::Point _vector (_x_1, _y_1);

    /* Tested function call */
    _cpptest_TestObject.translate(_vector);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_translate_4 */

/* CPPTEST_TEST_CASE_BEGIN test_translate_5 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::translate(const Point &) */
void TestSuite_Point_hpp_521bf514::test_translate_5()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = 1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMinInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (vector) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = 1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMinInt();
    ::Point _vector (_x_1, _y_1);

    /* Tested function call */
    _cpptest_TestObject.translate(_vector);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_translate_5 */

/* CPPTEST_TEST_CASE_BEGIN test_translate_6 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::translate(const Point &) */
void TestSuite_Point_hpp_521bf514::test_translate_6()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = -1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (vector) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = -1;
    ::Point _vector (_x_1, _y_1);

    /* Tested function call */
    _cpptest_TestObject.translate(_vector);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_translate_6 */

/* CPPTEST_TEST_CASE_BEGIN test_translate_7 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::translate(const Point &) */
void TestSuite_Point_hpp_521bf514::test_translate_7()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 0;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (vector) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = 1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = -1;
    ::Point _vector (_x_1, _y_1);

    /* Tested function call */
    _cpptest_TestObject.translate(_vector);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_translate_7 */

/* CPPTEST_TEST_CASE_BEGIN test_translate_8 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::translate(const Point &) */
void TestSuite_Point_hpp_521bf514::test_translate_8()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 1;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (vector) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = 1;
    ::Point _vector (_x_1, _y_1);

    /* Tested function call */
    _cpptest_TestObject.translate(_vector);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_translate_8 */

/* CPPTEST_TEST_CASE_BEGIN test_translate_9 */
/* CPPTEST_TEST_CASE_CONTEXT void Point::translate(const Point &) */
void TestSuite_Point_hpp_521bf514::test_translate_9()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = -1;
    ::Point _cpptest_TestObject (_x_0, _y_0);

    /* Initializing argument 1 (vector) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = 0;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = 1;
    ::Point _vector (_x_1, _y_1);

    /* Tested function call */
    _cpptest_TestObject.translate(_vector);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._x", ( _cpptest_TestObject._x ));
    CPPTEST_POST_CONDITION_INTEGER("int _cpptest_TestObject._y", ( _cpptest_TestObject._y ));
}
/* CPPTEST_TEST_CASE_END test_translate_9 */
