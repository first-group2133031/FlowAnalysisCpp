#include "cpptest.h"

/* CPPTEST_TEST_SUITE_CODE_BEGIN AdditionalIncludes */
/* CPPTEST_TEST_SUITE_CODE_END AdditionalIncludes */

CPPTEST_CONTEXT("/FlowAnalysisCpp/Shapes.hpp");
CPPTEST_TEST_SUITE_INCLUDED_TO("/FlowAnalysisCpp/DeadLock.cpp");

class TestSuite_Shapes_hpp_b09152df : public CppTest_TestSuite
{
    public:
        CPPTEST_TEST_SUITE(TestSuite_Shapes_hpp_b09152df);
        CPPTEST_TEST_SUITE_SETUP(testSuiteSetUp);
        CPPTEST_TEST(test_Circle_1);
        CPPTEST_TEST(test_Circle_10);
        CPPTEST_TEST(test_Circle_2);
        CPPTEST_TEST(test_Circle_3);
        CPPTEST_TEST(test_Circle_4);
        CPPTEST_TEST(test_Circle_5);
        CPPTEST_TEST(test_Circle_6);
        CPPTEST_TEST(test_Circle_7);
        CPPTEST_TEST(test_Circle_8);
        CPPTEST_TEST(test_Circle_9);
        CPPTEST_TEST(test_LineSegment_1);
        CPPTEST_TEST(test_LineSegment_10);
        CPPTEST_TEST(test_LineSegment_2);
        CPPTEST_TEST(test_LineSegment_3);
        CPPTEST_TEST(test_LineSegment_4);
        CPPTEST_TEST(test_LineSegment_5);
        CPPTEST_TEST(test_LineSegment_6);
        CPPTEST_TEST(test_LineSegment_7);
        CPPTEST_TEST(test_LineSegment_8);
        CPPTEST_TEST(test_LineSegment_9);
        CPPTEST_TEST(test_contains_1);
        CPPTEST_TEST(test_contains_10);
        CPPTEST_TEST(test_contains_2);
        CPPTEST_TEST(test_contains_3);
        CPPTEST_TEST(test_contains_4);
        CPPTEST_TEST(test_contains_5);
        CPPTEST_TEST(test_contains_6);
        CPPTEST_TEST(test_contains_7);
        CPPTEST_TEST(test_contains_8);
        CPPTEST_TEST(test_contains_9);
        CPPTEST_TEST(test_getArea_1);
        CPPTEST_TEST(test_getArea_10);
        CPPTEST_TEST(test_getArea_11);
        CPPTEST_TEST(test_getArea_12);
        CPPTEST_TEST(test_getArea_13);
        CPPTEST_TEST(test_getArea_14);
        CPPTEST_TEST(test_getArea_15);
        CPPTEST_TEST(test_getArea_16);
        CPPTEST_TEST(test_getArea_17);
        CPPTEST_TEST(test_getArea_18);
        CPPTEST_TEST(test_getArea_19);
        CPPTEST_TEST(test_getArea_2);
        CPPTEST_TEST(test_getArea_20);
        CPPTEST_TEST(test_getArea_3);
        CPPTEST_TEST(test_getArea_4);
        CPPTEST_TEST(test_getArea_5);
        CPPTEST_TEST(test_getArea_6);
        CPPTEST_TEST(test_getArea_7);
        CPPTEST_TEST(test_getArea_8);
        CPPTEST_TEST(test_getArea_9);
        CPPTEST_TEST_SUITE_TEARDOWN(testSuiteTearDown);
        CPPTEST_TEST_SUITE_END();
        
        static void testSuiteSetUp();
        static void testSuiteTearDown();

        void setUp();
        void tearDown();

        void test_Circle_1();
        void test_Circle_10();
        void test_Circle_2();
        void test_Circle_3();
        void test_Circle_4();
        void test_Circle_5();
        void test_Circle_6();
        void test_Circle_7();
        void test_Circle_8();
        void test_Circle_9();
        void test_LineSegment_1();
        void test_LineSegment_10();
        void test_LineSegment_2();
        void test_LineSegment_3();
        void test_LineSegment_4();
        void test_LineSegment_5();
        void test_LineSegment_6();
        void test_LineSegment_7();
        void test_LineSegment_8();
        void test_LineSegment_9();
        void test_contains_1();
        void test_contains_10();
        void test_contains_2();
        void test_contains_3();
        void test_contains_4();
        void test_contains_5();
        void test_contains_6();
        void test_contains_7();
        void test_contains_8();
        void test_contains_9();
        void test_getArea_1();
        void test_getArea_10();
        void test_getArea_11();
        void test_getArea_12();
        void test_getArea_13();
        void test_getArea_14();
        void test_getArea_15();
        void test_getArea_16();
        void test_getArea_17();
        void test_getArea_18();
        void test_getArea_19();
        void test_getArea_2();
        void test_getArea_20();
        void test_getArea_3();
        void test_getArea_4();
        void test_getArea_5();
        void test_getArea_6();
        void test_getArea_7();
        void test_getArea_8();
        void test_getArea_9();
};

CPPTEST_TEST_SUITE_REGISTRATION(TestSuite_Shapes_hpp_b09152df);

void TestSuite_Shapes_hpp_b09152df::testSuiteSetUp()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestSuiteSetUp */
/* CPPTEST_TEST_SUITE_CODE_END TestSuiteSetUp */
}

void TestSuite_Shapes_hpp_b09152df::testSuiteTearDown()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestSuiteTearDown */
/* CPPTEST_TEST_SUITE_CODE_END TestSuiteTearDown */
}

void TestSuite_Shapes_hpp_b09152df::setUp()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestCaseSetUp */
/* CPPTEST_TEST_SUITE_CODE_END TestCaseSetUp */
}

void TestSuite_Shapes_hpp_b09152df::tearDown()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestCaseTearDown */
/* CPPTEST_TEST_SUITE_CODE_END TestCaseTearDown */
}

/* CPPTEST_TEST_CASE_BEGIN test_Circle_1 */
/* CPPTEST_TEST_CASE_CONTEXT Circle::Circle(Point, double) */
void TestSuite_Shapes_hpp_b09152df::test_Circle_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (radius) */ 
    double _radius  = cpptestLimitsGetMaxPosDouble();
    /* Testing constructor. */
    ::Circle _return(_position, _radius);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return._radius", ( _return._radius ));
}
/* CPPTEST_TEST_CASE_END test_Circle_1 */

/* CPPTEST_TEST_CASE_BEGIN test_Circle_10 */
/* CPPTEST_TEST_CASE_CONTEXT Circle::Circle(Point, double) */
void TestSuite_Shapes_hpp_b09152df::test_Circle_10()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMinInt();
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (radius) */ 
    double _radius  = 0.0;
    /* Testing constructor. */
    ::Circle _return(_position, _radius);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return._radius", ( _return._radius ));
}
/* CPPTEST_TEST_CASE_END test_Circle_10 */

/* CPPTEST_TEST_CASE_BEGIN test_Circle_2 */
/* CPPTEST_TEST_CASE_CONTEXT Circle::Circle(Point, double) */
void TestSuite_Shapes_hpp_b09152df::test_Circle_2()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (radius) */ 
    double _radius  = cpptestLimitsGetMaxPosDouble();
    /* Testing constructor. */
    ::Circle _return(_position, _radius);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return._radius", ( _return._radius ));
}
/* CPPTEST_TEST_CASE_END test_Circle_2 */

/* CPPTEST_TEST_CASE_BEGIN test_Circle_3 */
/* CPPTEST_TEST_CASE_CONTEXT Circle::Circle(Point, double) */
void TestSuite_Shapes_hpp_b09152df::test_Circle_3()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 0;
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (radius) */ 
    double _radius  = cpptestLimitsGetMaxPosDouble();
    /* Testing constructor. */
    ::Circle _return(_position, _radius);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return._radius", ( _return._radius ));
}
/* CPPTEST_TEST_CASE_END test_Circle_3 */

/* CPPTEST_TEST_CASE_BEGIN test_Circle_4 */
/* CPPTEST_TEST_CASE_CONTEXT Circle::Circle(Point, double) */
void TestSuite_Shapes_hpp_b09152df::test_Circle_4()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = 0;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = -1;
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (radius) */ 
    double _radius  = cpptestLimitsGetMaxNegDouble();
    /* Testing constructor. */
    ::Circle _return(_position, _radius);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return._radius", ( _return._radius ));
}
/* CPPTEST_TEST_CASE_END test_Circle_4 */

/* CPPTEST_TEST_CASE_BEGIN test_Circle_5 */
/* CPPTEST_TEST_CASE_CONTEXT Circle::Circle(Point, double) */
void TestSuite_Shapes_hpp_b09152df::test_Circle_5()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = 1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMinInt();
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (radius) */ 
    double _radius  = cpptestLimitsGetMinNegDouble();
    /* Testing constructor. */
    ::Circle _return(_position, _radius);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return._radius", ( _return._radius ));
}
/* CPPTEST_TEST_CASE_END test_Circle_5 */

/* CPPTEST_TEST_CASE_BEGIN test_Circle_6 */
/* CPPTEST_TEST_CASE_CONTEXT Circle::Circle(Point, double) */
void TestSuite_Shapes_hpp_b09152df::test_Circle_6()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = -1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (radius) */ 
    double _radius  = -1.0;
    /* Testing constructor. */
    ::Circle _return(_position, _radius);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return._radius", ( _return._radius ));
}
/* CPPTEST_TEST_CASE_END test_Circle_6 */

/* CPPTEST_TEST_CASE_BEGIN test_Circle_7 */
/* CPPTEST_TEST_CASE_CONTEXT Circle::Circle(Point, double) */
void TestSuite_Shapes_hpp_b09152df::test_Circle_7()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 0;
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (radius) */ 
    double _radius  = -1.0;
    /* Testing constructor. */
    ::Circle _return(_position, _radius);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return._radius", ( _return._radius ));
}
/* CPPTEST_TEST_CASE_END test_Circle_7 */

/* CPPTEST_TEST_CASE_BEGIN test_Circle_8 */
/* CPPTEST_TEST_CASE_CONTEXT Circle::Circle(Point, double) */
void TestSuite_Shapes_hpp_b09152df::test_Circle_8()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 1;
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (radius) */ 
    double _radius  = cpptestLimitsGetMinPosDouble();
    /* Testing constructor. */
    ::Circle _return(_position, _radius);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return._radius", ( _return._radius ));
}
/* CPPTEST_TEST_CASE_END test_Circle_8 */

/* CPPTEST_TEST_CASE_BEGIN test_Circle_9 */
/* CPPTEST_TEST_CASE_CONTEXT Circle::Circle(Point, double) */
void TestSuite_Shapes_hpp_b09152df::test_Circle_9()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = -1;
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (radius) */ 
    double _radius  = 1.0;
    /* Testing constructor. */
    ::Circle _return(_position, _radius);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return._radius", ( _return._radius ));
}
/* CPPTEST_TEST_CASE_END test_Circle_9 */

/* CPPTEST_TEST_CASE_BEGIN test_LineSegment_1 */
/* CPPTEST_TEST_CASE_CONTEXT LineSegment::LineSegment(Point, Point) */
void TestSuite_Shapes_hpp_b09152df::test_LineSegment_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (end) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMaxInt();
    ::Point _end (_x_1, _y_1);

    /* Testing constructor. */
    ::LineSegment _return(_position, _end);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_LineSegment_1 */

/* CPPTEST_TEST_CASE_BEGIN test_LineSegment_10 */
/* CPPTEST_TEST_CASE_CONTEXT LineSegment::LineSegment(Point, Point) */
void TestSuite_Shapes_hpp_b09152df::test_LineSegment_10()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMinInt();
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (end) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = -1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = 0;
    ::Point _end (_x_1, _y_1);

    /* Testing constructor. */
    ::LineSegment _return(_position, _end);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_LineSegment_10 */

/* CPPTEST_TEST_CASE_BEGIN test_LineSegment_2 */
/* CPPTEST_TEST_CASE_CONTEXT LineSegment::LineSegment(Point, Point) */
void TestSuite_Shapes_hpp_b09152df::test_LineSegment_2()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (end) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMaxInt();
    ::Point _end (_x_1, _y_1);

    /* Testing constructor. */
    ::LineSegment _return(_position, _end);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_LineSegment_2 */

/* CPPTEST_TEST_CASE_BEGIN test_LineSegment_3 */
/* CPPTEST_TEST_CASE_CONTEXT LineSegment::LineSegment(Point, Point) */
void TestSuite_Shapes_hpp_b09152df::test_LineSegment_3()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 0;
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (end) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = -1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMaxInt();
    ::Point _end (_x_1, _y_1);

    /* Testing constructor. */
    ::LineSegment _return(_position, _end);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_LineSegment_3 */

/* CPPTEST_TEST_CASE_BEGIN test_LineSegment_4 */
/* CPPTEST_TEST_CASE_CONTEXT LineSegment::LineSegment(Point, Point) */
void TestSuite_Shapes_hpp_b09152df::test_LineSegment_4()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = 0;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = -1;
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (end) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMinInt();
    ::Point _end (_x_1, _y_1);

    /* Testing constructor. */
    ::LineSegment _return(_position, _end);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_LineSegment_4 */

/* CPPTEST_TEST_CASE_BEGIN test_LineSegment_5 */
/* CPPTEST_TEST_CASE_CONTEXT LineSegment::LineSegment(Point, Point) */
void TestSuite_Shapes_hpp_b09152df::test_LineSegment_5()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = 1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMinInt();
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (end) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = 1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = cpptestLimitsGetMinInt();
    ::Point _end (_x_1, _y_1);

    /* Testing constructor. */
    ::LineSegment _return(_position, _end);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_LineSegment_5 */

/* CPPTEST_TEST_CASE_BEGIN test_LineSegment_6 */
/* CPPTEST_TEST_CASE_CONTEXT LineSegment::LineSegment(Point, Point) */
void TestSuite_Shapes_hpp_b09152df::test_LineSegment_6()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = -1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = cpptestLimitsGetMaxInt();
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (end) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = -1;
    ::Point _end (_x_1, _y_1);

    /* Testing constructor. */
    ::LineSegment _return(_position, _end);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_LineSegment_6 */

/* CPPTEST_TEST_CASE_BEGIN test_LineSegment_7 */
/* CPPTEST_TEST_CASE_CONTEXT LineSegment::LineSegment(Point, Point) */
void TestSuite_Shapes_hpp_b09152df::test_LineSegment_7()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 0;
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (end) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = 1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = -1;
    ::Point _end (_x_1, _y_1);

    /* Testing constructor. */
    ::LineSegment _return(_position, _end);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_LineSegment_7 */

/* CPPTEST_TEST_CASE_BEGIN test_LineSegment_8 */
/* CPPTEST_TEST_CASE_CONTEXT LineSegment::LineSegment(Point, Point) */
void TestSuite_Shapes_hpp_b09152df::test_LineSegment_8()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = 1;
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (end) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = 1;
    ::Point _end (_x_1, _y_1);

    /* Testing constructor. */
    ::LineSegment _return(_position, _end);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_LineSegment_8 */

/* CPPTEST_TEST_CASE_BEGIN test_LineSegment_9 */
/* CPPTEST_TEST_CASE_CONTEXT LineSegment::LineSegment(Point, Point) */
void TestSuite_Shapes_hpp_b09152df::test_LineSegment_9()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (position) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_0  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_0  = -1;
    ::Point _position (_x_0, _y_0);

    /* Initializing argument 2 (end) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_1  = 0;
        /* Initializing constructor argument 2 (y) */ 
        int _y_1  = 1;
    ::Point _end (_x_1, _y_1);

    /* Testing constructor. */
    ::LineSegment _return(_position, _end);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_LineSegment_9 */

/* CPPTEST_TEST_CASE_BEGIN test_contains_1 */
/* CPPTEST_TEST_CASE_CONTEXT int Circle::contains(Point) */
void TestSuite_Shapes_hpp_b09152df::test_contains_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMaxInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMaxInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = cpptestLimitsGetMaxPosDouble();
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_2  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_2  = cpptestLimitsGetMaxInt();
    ::Point _point (_x_2, _y_2);

    /* Tested function call */
    int _return  = _cpptest_TestObject.contains(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_contains_1 */

/* CPPTEST_TEST_CASE_BEGIN test_contains_10 */
/* CPPTEST_TEST_CASE_CONTEXT int Circle::contains(Point) */
void TestSuite_Shapes_hpp_b09152df::test_contains_10()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = 0;
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = -1;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = cpptestLimitsGetMaxNegDouble();
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_2  = -1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_2  = 0;
    ::Point _point (_x_2, _y_2);

    /* Tested function call */
    int _return  = _cpptest_TestObject.contains(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_contains_10 */

/* CPPTEST_TEST_CASE_BEGIN test_contains_2 */
/* CPPTEST_TEST_CASE_CONTEXT int Circle::contains(Point) */
void TestSuite_Shapes_hpp_b09152df::test_contains_2()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMinInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMaxInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = cpptestLimitsGetMaxPosDouble();
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_2  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_2  = cpptestLimitsGetMaxInt();
    ::Point _point (_x_2, _y_2);

    /* Tested function call */
    int _return  = _cpptest_TestObject.contains(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_contains_2 */

/* CPPTEST_TEST_CASE_BEGIN test_contains_3 */
/* CPPTEST_TEST_CASE_CONTEXT int Circle::contains(Point) */
void TestSuite_Shapes_hpp_b09152df::test_contains_3()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = -1;
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = -1;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = 1.0;
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_2  = -1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_2  = cpptestLimitsGetMaxInt();
    ::Point _point (_x_2, _y_2);

    /* Tested function call */
    int _return  = _cpptest_TestObject.contains(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_contains_3 */

/* CPPTEST_TEST_CASE_BEGIN test_contains_4 */
/* CPPTEST_TEST_CASE_CONTEXT int Circle::contains(Point) */
void TestSuite_Shapes_hpp_b09152df::test_contains_4()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = 1;
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = 0;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = -1.0;
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_2  = cpptestLimitsGetMaxInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_2  = cpptestLimitsGetMinInt();
    ::Point _point (_x_2, _y_2);

    /* Tested function call */
    int _return  = _cpptest_TestObject.contains(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_contains_4 */

/* CPPTEST_TEST_CASE_BEGIN test_contains_5 */
/* CPPTEST_TEST_CASE_CONTEXT int Circle::contains(Point) */
void TestSuite_Shapes_hpp_b09152df::test_contains_5()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = 0;
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMinInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = cpptestLimitsGetMinNegDouble();
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_2  = 1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_2  = cpptestLimitsGetMinInt();
    ::Point _point (_x_2, _y_2);

    /* Tested function call */
    int _return  = _cpptest_TestObject.contains(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_contains_5 */

/* CPPTEST_TEST_CASE_BEGIN test_contains_6 */
/* CPPTEST_TEST_CASE_CONTEXT int Circle::contains(Point) */
void TestSuite_Shapes_hpp_b09152df::test_contains_6()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMaxInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = 0;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = cpptestLimitsGetMaxPosDouble();
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_2  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_2  = -1;
    ::Point _point (_x_2, _y_2);

    /* Tested function call */
    int _return  = _cpptest_TestObject.contains(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_contains_6 */

/* CPPTEST_TEST_CASE_BEGIN test_contains_7 */
/* CPPTEST_TEST_CASE_CONTEXT int Circle::contains(Point) */
void TestSuite_Shapes_hpp_b09152df::test_contains_7()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMinInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMinInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = 0.0;
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_2  = 1;
        /* Initializing constructor argument 2 (y) */ 
        int _y_2  = -1;
    ::Point _point (_x_2, _y_2);

    /* Tested function call */
    int _return  = _cpptest_TestObject.contains(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_contains_7 */

/* CPPTEST_TEST_CASE_BEGIN test_contains_8 */
/* CPPTEST_TEST_CASE_CONTEXT int Circle::contains(Point) */
void TestSuite_Shapes_hpp_b09152df::test_contains_8()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = -1;
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = 1;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = cpptestLimitsGetMinPosDouble();
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_2  = cpptestLimitsGetMinInt();
        /* Initializing constructor argument 2 (y) */ 
        int _y_2  = 1;
    ::Point _point (_x_2, _y_2);

    /* Tested function call */
    int _return  = _cpptest_TestObject.contains(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_contains_8 */

/* CPPTEST_TEST_CASE_BEGIN test_contains_9 */
/* CPPTEST_TEST_CASE_CONTEXT int Circle::contains(Point) */
void TestSuite_Shapes_hpp_b09152df::test_contains_9()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = 1;
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMaxInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = -1.0;
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Initializing argument 1 (point) */ 
        /* Initializing constructor argument 1 (x) */ 
        int _x_2  = 0;
        /* Initializing constructor argument 2 (y) */ 
        int _y_2  = 1;
    ::Point _point (_x_2, _y_2);

    /* Tested function call */
    int _return  = _cpptest_TestObject.contains(_point);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_contains_9 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_1 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double LineSegment::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMaxInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMaxInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (end) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_2  = cpptestLimitsGetMaxInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_2  = cpptestLimitsGetMaxInt();
        ::Point _end_0 (_x_2, _y_2);

    ::LineSegment _cpptest_TestObject (_position_0, _end_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
}
/* CPPTEST_TEST_CASE_END test_getArea_1 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_10 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double LineSegment::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_10()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMaxInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMinInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (end) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_2  = -1;
            /* Initializing constructor argument 2 (y) */ 
            int _y_2  = 0;
        ::Point _end_0 (_x_2, _y_2);

    ::LineSegment _cpptest_TestObject (_position_0, _end_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
}
/* CPPTEST_TEST_CASE_END test_getArea_10 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_11 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double Circle::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_11()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMaxInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMaxInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = 3.14159;
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_getArea_11 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_12 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double Circle::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_12()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMinInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMaxInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = 3.14159;
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_getArea_12 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_13 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double Circle::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_13()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = 1;
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = 0;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = 3.14159;
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_getArea_13 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_14 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double Circle::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_14()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMaxInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = 0;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = cpptestLimitsGetMaxPosDouble();
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_getArea_14 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_15 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double Circle::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_15()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = -1;
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = 1;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = cpptestLimitsGetMaxNegDouble();
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_getArea_15 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_16 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double Circle::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_16()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = 0;
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = -1;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = cpptestLimitsGetMinNegDouble();
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_getArea_16 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_17 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double Circle::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_17()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMinInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = -1;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = -1.0;
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_getArea_17 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_18 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double Circle::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_18()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = 1;
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMinInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = cpptestLimitsGetMinPosDouble();
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_getArea_18 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_19 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double Circle::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_19()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMaxInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMinInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = 1.0;
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_getArea_19 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_2 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double LineSegment::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_2()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMinInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMaxInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (end) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_2  = cpptestLimitsGetMaxInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_2  = cpptestLimitsGetMaxInt();
        ::Point _end_0 (_x_2, _y_2);

    ::LineSegment _cpptest_TestObject (_position_0, _end_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
}
/* CPPTEST_TEST_CASE_END test_getArea_2 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_20 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double Circle::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_20()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = -1;
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMaxInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (radius) */ 
        double _radius_0  = 0.0;
    ::Circle _cpptest_TestObject (_position_0, _radius_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
    CPPTEST_POST_CONDITION_FLOAT("double _cpptest_TestObject._radius", ( _cpptest_TestObject._radius ));
}
/* CPPTEST_TEST_CASE_END test_getArea_20 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_3 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double LineSegment::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_3()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMaxInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = 0;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (end) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_2  = -1;
            /* Initializing constructor argument 2 (y) */ 
            int _y_2  = cpptestLimitsGetMaxInt();
        ::Point _end_0 (_x_2, _y_2);

    ::LineSegment _cpptest_TestObject (_position_0, _end_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
}
/* CPPTEST_TEST_CASE_END test_getArea_3 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_4 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double LineSegment::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_4()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = 0;
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = -1;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (end) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_2  = cpptestLimitsGetMaxInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_2  = cpptestLimitsGetMinInt();
        ::Point _end_0 (_x_2, _y_2);

    ::LineSegment _cpptest_TestObject (_position_0, _end_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
}
/* CPPTEST_TEST_CASE_END test_getArea_4 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_5 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double LineSegment::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_5()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = 1;
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMinInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (end) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_2  = 1;
            /* Initializing constructor argument 2 (y) */ 
            int _y_2  = cpptestLimitsGetMinInt();
        ::Point _end_0 (_x_2, _y_2);

    ::LineSegment _cpptest_TestObject (_position_0, _end_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
}
/* CPPTEST_TEST_CASE_END test_getArea_5 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_6 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double LineSegment::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_6()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = -1;
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = cpptestLimitsGetMaxInt();
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (end) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_2  = cpptestLimitsGetMinInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_2  = -1;
        ::Point _end_0 (_x_2, _y_2);

    ::LineSegment _cpptest_TestObject (_position_0, _end_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
}
/* CPPTEST_TEST_CASE_END test_getArea_6 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_7 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double LineSegment::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_7()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMinInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = 0;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (end) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_2  = 1;
            /* Initializing constructor argument 2 (y) */ 
            int _y_2  = -1;
        ::Point _end_0 (_x_2, _y_2);

    ::LineSegment _cpptest_TestObject (_position_0, _end_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
}
/* CPPTEST_TEST_CASE_END test_getArea_7 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_8 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double LineSegment::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_8()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMaxInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = 1;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (end) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_2  = cpptestLimitsGetMinInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_2  = 1;
        ::Point _end_0 (_x_2, _y_2);

    ::LineSegment _cpptest_TestObject (_position_0, _end_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
}
/* CPPTEST_TEST_CASE_END test_getArea_8 */

/* CPPTEST_TEST_CASE_BEGIN test_getArea_9 */
/* CPPTEST_TEST_CASE_CONTEXT virtual double LineSegment::getArea(void) const */
void TestSuite_Shapes_hpp_b09152df::test_getArea_9()
{
    /* Pre-condition initialization */
    /* Initializing argument 0 (this) */ 
        /* Initializing constructor argument 1 (position) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_1  = cpptestLimitsGetMaxInt();
            /* Initializing constructor argument 2 (y) */ 
            int _y_1  = -1;
        ::Point _position_0 (_x_1, _y_1);

        /* Initializing constructor argument 2 (end) */ 
            /* Initializing constructor argument 1 (x) */ 
            int _x_2  = 0;
            /* Initializing constructor argument 2 (y) */ 
            int _y_2  = 1;
        ::Point _end_0 (_x_2, _y_2);

    ::LineSegment _cpptest_TestObject (_position_0, _end_0);

    /* Tested function call */
    double _return  = _cpptest_TestObject.getArea();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
}
/* CPPTEST_TEST_CASE_END test_getArea_9 */
