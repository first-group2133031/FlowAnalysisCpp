#include "cpptest.h"

/* CPPTEST_TEST_SUITE_CODE_BEGIN AdditionalIncludes */
/* CPPTEST_TEST_SUITE_CODE_END AdditionalIncludes */

CPPTEST_CONTEXT("/FlowAnalysisCpp/MemoryLeak.cpp");
CPPTEST_TEST_SUITE_INCLUDED_TO("/FlowAnalysisCpp/MemoryLeak.cpp");

class TestSuite_MemoryLeak_cpp_2b9e8ccf : public CppTest_TestSuite
{
    public:
        CPPTEST_TEST_SUITE(TestSuite_MemoryLeak_cpp_2b9e8ccf);
        CPPTEST_TEST_SUITE_SETUP(testSuiteSetUp);
        CPPTEST_TEST(test_readIntegerArray_1);
        CPPTEST_TEST(test_readIntegerArray_2);
        CPPTEST_TEST(test_readIntegerArray_3);
        CPPTEST_TEST(test_readIntegerArray_4);
        CPPTEST_TEST(test_readIntegerArray_5);
        CPPTEST_TEST(test_readIntegerArray_6);
        CPPTEST_TEST(test_readIntegerArray_7);
        CPPTEST_TEST(test_readIntegerArray_8);
        CPPTEST_TEST_SUITE_TEARDOWN(testSuiteTearDown);
        CPPTEST_TEST_SUITE_END();
        
        static void testSuiteSetUp();
        static void testSuiteTearDown();

        void setUp();
        void tearDown();

        void test_readIntegerArray_1();
        void test_readIntegerArray_2();
        void test_readIntegerArray_3();
        void test_readIntegerArray_4();
        void test_readIntegerArray_5();
        void test_readIntegerArray_6();
        void test_readIntegerArray_7();
        void test_readIntegerArray_8();
};

CPPTEST_TEST_SUITE_REGISTRATION(TestSuite_MemoryLeak_cpp_2b9e8ccf);

void TestSuite_MemoryLeak_cpp_2b9e8ccf::testSuiteSetUp()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestSuiteSetUp */
/* CPPTEST_TEST_SUITE_CODE_END TestSuiteSetUp */
}

void TestSuite_MemoryLeak_cpp_2b9e8ccf::testSuiteTearDown()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestSuiteTearDown */
/* CPPTEST_TEST_SUITE_CODE_END TestSuiteTearDown */
}

void TestSuite_MemoryLeak_cpp_2b9e8ccf::setUp()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestCaseSetUp */
/* CPPTEST_TEST_SUITE_CODE_END TestCaseSetUp */
}

void TestSuite_MemoryLeak_cpp_2b9e8ccf::tearDown()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestCaseTearDown */
/* CPPTEST_TEST_SUITE_CODE_END TestCaseTearDown */
}

/* CPPTEST_TEST_CASE_BEGIN test_readIntegerArray_1 */
/* CPPTEST_TEST_CASE_CONTEXT int * readIntegerArray(FILE *, int *) */
void TestSuite_MemoryLeak_cpp_2b9e8ccf::test_readIntegerArray_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (file) */ 
    ::FILE * _file  = 0 ;
    /* Initializing argument 2 (pSize) */ 
    int _pSize_0 [1];
    int * _pSize  = (int * )cpptestMemset((void*)&_pSize_0, 0, (1 * sizeof(int)));
    /* Tested function call */
    int * _return  = ::readIntegerArray(_file, _pSize);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _return", ( _return ), sizeof(int));
    CPPTEST_POST_CONDITION_PTR("FILE * _file ", ( _file ));
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _pSize", ( _pSize ), sizeof(int));
}
/* CPPTEST_TEST_CASE_END test_readIntegerArray_1 */

/* CPPTEST_TEST_CASE_BEGIN test_readIntegerArray_2 */
/* CPPTEST_TEST_CASE_CONTEXT int * readIntegerArray(FILE *, int *) */
void TestSuite_MemoryLeak_cpp_2b9e8ccf::test_readIntegerArray_2()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (file) */ 
    ::FILE _file_0 ;
    ::FILE * _file  = & _file_0;
    /* Initializing argument 2 (pSize) */ 
    int _pSize_1 [1];
    int * _pSize  = (int * )cpptestMemset((void*)&_pSize_1, 0, (1 * sizeof(int)));
    /* Tested function call */
    int * _return  = ::readIntegerArray(_file, _pSize);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _return", ( _return ), sizeof(int));
    CPPTEST_POST_CONDITION_PTR("FILE * _file ", ( _file ));
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _pSize", ( _pSize ), sizeof(int));
}
/* CPPTEST_TEST_CASE_END test_readIntegerArray_2 */

/* CPPTEST_TEST_CASE_BEGIN test_readIntegerArray_3 */
/* CPPTEST_TEST_CASE_CONTEXT int * readIntegerArray(FILE *, int *) */
void TestSuite_MemoryLeak_cpp_2b9e8ccf::test_readIntegerArray_3()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (file) */ 
    ::FILE * _file  = 0 ;
    /* Initializing argument 2 (pSize) */ 
    int _pSize_0 [16];
    int * _pSize  = (int * )cpptestMemset((void*)&_pSize_0, 0, (16 * sizeof(int)));
    /* Tested function call */
    int * _return  = ::readIntegerArray(_file, _pSize);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _return", ( _return ), sizeof(int));
    CPPTEST_POST_CONDITION_PTR("FILE * _file ", ( _file ));
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _pSize", ( _pSize ), sizeof(int));
}
/* CPPTEST_TEST_CASE_END test_readIntegerArray_3 */

/* CPPTEST_TEST_CASE_BEGIN test_readIntegerArray_4 */
/* CPPTEST_TEST_CASE_CONTEXT int * readIntegerArray(FILE *, int *) */
void TestSuite_MemoryLeak_cpp_2b9e8ccf::test_readIntegerArray_4()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (file) */ 
    ::FILE _file_0 ;
    ::FILE * _file  = & _file_0;
    /* Initializing argument 2 (pSize) */ 
    int _pSize_1 [16];
    int * _pSize  = (int * )cpptestMemset((void*)&_pSize_1, 0, (16 * sizeof(int)));
    /* Tested function call */
    int * _return  = ::readIntegerArray(_file, _pSize);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _return", ( _return ), sizeof(int));
    CPPTEST_POST_CONDITION_PTR("FILE * _file ", ( _file ));
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _pSize", ( _pSize ), sizeof(int));
}
/* CPPTEST_TEST_CASE_END test_readIntegerArray_4 */

/* CPPTEST_TEST_CASE_BEGIN test_readIntegerArray_5 */
/* CPPTEST_TEST_CASE_CONTEXT int * readIntegerArray(FILE *, int *) */
void TestSuite_MemoryLeak_cpp_2b9e8ccf::test_readIntegerArray_5()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (file) */ 
    ::FILE * _file  = 0 ;
    /* Initializing argument 2 (pSize) */ 
    int _pSize_0 [256];
    int * _pSize  = (int * )cpptestMemset((void*)&_pSize_0, 0, (256 * sizeof(int)));
    /* Tested function call */
    int * _return  = ::readIntegerArray(_file, _pSize);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _return", ( _return ), sizeof(int));
    CPPTEST_POST_CONDITION_PTR("FILE * _file ", ( _file ));
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _pSize", ( _pSize ), sizeof(int));
}
/* CPPTEST_TEST_CASE_END test_readIntegerArray_5 */

/* CPPTEST_TEST_CASE_BEGIN test_readIntegerArray_6 */
/* CPPTEST_TEST_CASE_CONTEXT int * readIntegerArray(FILE *, int *) */
void TestSuite_MemoryLeak_cpp_2b9e8ccf::test_readIntegerArray_6()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (file) */ 
    ::FILE _file_0 ;
    ::FILE * _file  = & _file_0;
    /* Initializing argument 2 (pSize) */ 
    int _pSize_1 [256];
    int * _pSize  = (int * )cpptestMemset((void*)&_pSize_1, 0, (256 * sizeof(int)));
    /* Tested function call */
    int * _return  = ::readIntegerArray(_file, _pSize);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _return", ( _return ), sizeof(int));
    CPPTEST_POST_CONDITION_PTR("FILE * _file ", ( _file ));
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _pSize", ( _pSize ), sizeof(int));
}
/* CPPTEST_TEST_CASE_END test_readIntegerArray_6 */

/* CPPTEST_TEST_CASE_BEGIN test_readIntegerArray_7 */
/* CPPTEST_TEST_CASE_CONTEXT int * readIntegerArray(FILE *, int *) */
void TestSuite_MemoryLeak_cpp_2b9e8ccf::test_readIntegerArray_7()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (file) */ 
    ::FILE * _file  = 0 ;
    /* Initializing argument 2 (pSize) */ 
    int * _pSize  = 0 ;
    /* Tested function call */
    int * _return  = ::readIntegerArray(_file, _pSize);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _return", ( _return ), sizeof(int));
    CPPTEST_POST_CONDITION_PTR("FILE * _file ", ( _file ));
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _pSize", ( _pSize ), sizeof(int));
}
/* CPPTEST_TEST_CASE_END test_readIntegerArray_7 */

/* CPPTEST_TEST_CASE_BEGIN test_readIntegerArray_8 */
/* CPPTEST_TEST_CASE_CONTEXT int * readIntegerArray(FILE *, int *) */
void TestSuite_MemoryLeak_cpp_2b9e8ccf::test_readIntegerArray_8()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (file) */ 
    ::FILE _file_0 ;
    ::FILE * _file  = & _file_0;
    /* Initializing argument 2 (pSize) */ 
    int * _pSize  = 0 ;
    /* Tested function call */
    int * _return  = ::readIntegerArray(_file, _pSize);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _return", ( _return ), sizeof(int));
    CPPTEST_POST_CONDITION_PTR("FILE * _file ", ( _file ));
    CPPTEST_POST_CONDITION_MEM_BUFFER("int * _pSize", ( _pSize ), sizeof(int));
}
/* CPPTEST_TEST_CASE_END test_readIntegerArray_8 */
